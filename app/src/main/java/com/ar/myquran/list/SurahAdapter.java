package com.ar.myquran.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ar.myquran.R;
import com.ar.myquran.data.Surah;

import java.util.ArrayList;
import java.util.List;

public class SurahAdapter extends RecyclerView.Adapter<SurahAdapter.SurahViewHolder> {
    private List<Surah> surahs = new ArrayList<>();
    public SurahAdapter(List<Surah> surahs){
        this.surahs = surahs;
    }
    @NonNull
    @Override
    public SurahViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_surah,parent,false);
        return new SurahViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SurahViewHolder surahViewHolder, int position) {
        surahViewHolder.bindSurah(surahs.get(position));
    }

    @Override
    public int getItemCount() {
        return surahs.size();
    }

    public class SurahViewHolder extends RecyclerView.ViewHolder {
        private TextView surahNameTv;
        private TextView surahTypeTv;
        public SurahViewHolder(@NonNull View itemView) {
            super(itemView);
            surahNameTv = itemView.findViewById(R.id.tv_list_surahName);
            surahTypeTv = itemView.findViewById(R.id.tv_list_type);
        }

        public void bindSurah(Surah surah) {
            surahNameTv.setText(surah.getName());
            surahTypeTv.setText(surah.getNozol());
        }
    }
}
