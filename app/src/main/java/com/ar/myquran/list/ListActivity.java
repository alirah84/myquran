package com.ar.myquran.list;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ar.myquran.R;
import com.ar.myquran.data.AppDatabase;
import com.ar.myquran.data.Surah;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {
    private List<Surah> surahs = new ArrayList<>();
    private RecyclerView surahRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        AppDatabase appDatabase = new AppDatabase(this);
      surahs = appDatabase.getAllSurah();
      surahRv = findViewById(R.id.rv_list_surah);
      surahRv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
      surahRv.setAdapter(new SurahAdapter(surahs));
    }
}
