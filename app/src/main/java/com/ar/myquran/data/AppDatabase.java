package com.ar.myquran.data;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class AppDatabase extends SQLiteOpenHelper {
    final private static int DATABASE_VERSION = 1;
    final private static String DATABASE_NAME = "myquran.db";

    private AssetManager assets;
    private String databaseDir;

    public AppDatabase(Context context) {
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
        assets = context.getAssets();
        databaseDir = context.getApplicationInfo().dataDir + "/databases/";

        File file = new File(databaseDir);
        if(!file.exists()) file.mkdir();
    }

    @Override
    public SQLiteDatabase getReadableDatabase() {
        if (!isDatabaseExist()){
            copyDatabase();
        }
        return super.getReadableDatabase();
    }

    private Boolean isDatabaseExist() {
        return new File(databaseDir + DATABASE_NAME).exists();
    }

    private void copyDatabase() {

        try {
            InputStream inputStream = assets.open("databases/" + DATABASE_NAME);

            FileOutputStream outputStream = new FileOutputStream(databaseDir + DATABASE_NAME);

            byte[] buffer = new byte[8 * 1024];

            int readed;
            while ((readed = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, readed);
            }

            outputStream.flush();

            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public List<Surah> getAllSurah(){
        List<Surah> surahs = new ArrayList<>();
       SQLiteDatabase db = getReadableDatabase();
     Cursor cursor = db.rawQuery("SELECT * FROM Surah",null);
     if(cursor.moveToFirst()){
         do {
             Surah surah = new Surah();
            surah.setId(cursor.getInt(0));
            surah.setName(cursor.getString(1));
            surah.setNozol(cursor.getString(2));
            surah.setVerse(cursor.getInt(3));
            surah.setRevel(cursor.getInt(4));
            surah.setBerab(cursor.getString(4));
            surahs.add(surah);
         }
         while (cursor.moveToNext());
     }
     cursor.close();
     return surahs;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
