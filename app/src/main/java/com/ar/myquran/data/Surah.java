package com.ar.myquran.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Surah implements Parcelable {
    private int Id;
    private String Name;
    private String Nozol;
    private String Berab;
    private int Verse;
    private int revel;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNozol() {
        return Nozol;
    }

    public void setNozol(String nozol) {
        Nozol = nozol;
    }

    public String getBerab() {
        return Berab;
    }

    public void setBerab(String berab) {
        Berab = berab;
    }

    public int getVerse() {
        return Verse;
    }

    public void setVerse(int verse) {
        Verse = verse;
    }

    public int getRevel() {
        return revel;
    }

    public void setRevel(int revel) {
        this.revel = revel;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Id);
        dest.writeString(this.Name);
        dest.writeString(this.Nozol);
        dest.writeString(this.Berab);
        dest.writeInt(this.Verse);
        dest.writeInt(this.revel);
    }

    public Surah() {
    }

    protected Surah(Parcel in) {
        this.Id = in.readInt();
        this.Name = in.readString();
        this.Nozol = in.readString();
        this.Berab = in.readString();
        this.Verse = in.readInt();
        this.revel = in.readInt();
    }

    public static final Parcelable.Creator<Surah> CREATOR = new Parcelable.Creator<Surah>() {
        @Override
        public Surah createFromParcel(Parcel source) {
            return new Surah(source);
        }

        @Override
        public Surah[] newArray(int size) {
            return new Surah[size];
        }
    };
}
